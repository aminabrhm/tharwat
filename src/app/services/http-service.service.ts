import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';
import{AuthService} from './auth.service';

import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Donations_info } from 'src/app/models/donations';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  headers = new HttpHeaders();
  isLoggedIn = false;

  constructor(
    private http : HttpClient,
    private env: EnvService,
    private authService: AuthService,
    private storage: NativeStorage,
    ) { }

    // token = this.authService.token;
    token=this.storage.getItem('token')

  public makePost( endpoint : string, data : any) {
    let options = {headers: new HttpHeaders({ 
      'Authorization': "Bearer "+ this.storage.getItem('token'),
      // 'Authorization': "Bearer "+ this.token.access_token,
      'Content-Type': 'application/json',
     })  };
    return this.http.post(endpoint,
    data, options).pipe(tap(
     res =>{
       console.log(res);
     },
     err => {
       console.log(err.message);
     }
    )
   );
  }
  
  

  public makeGet( endpoint : string) {
    const headers = new HttpHeaders({
      'Authorization': "Bearer "+this.token
    });
    return this.http.get<any>(this.env.API_URL + endpoint, { headers: headers })  
    .pipe(
      tap(
        data => {
        return data
        }
      )
    )
  }


  public getHeaders(authenticated : boolean){
    let headers = new Headers();
    let token = localStorage.getItem('token');
    headers.append( 'Content-Type', 'application/json' );
    if ( authenticated ) {
      headers.append( 'Authorization', token);
    }
    return headers;
  }
}
